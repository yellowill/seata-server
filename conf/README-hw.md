参考：https://www.jianshu.com/p/223ad263a822

1.mysql数据库新建库seata库及seata库建表
![img_1.png](img_1.png)

2.把config.txt中的文件导入都Nacos的配置中心里，需要进入到config-center目录下的nacos文件夹，里面有两个文件，一个是.sh文件，一个是.py文件，当然也就提供了两种方式导入Nacos配置中心。下面使用.sh文件，在nacos文件下打开git bash终端命令行，执行如下命令：
config.txt的修改内容关注下面几点
![img_2.png](img_2.png)
![img_3.png](img_3.png)


sh nacos-config.sh -h 127.0.0.1 -p 8848 -g SEATA_GROUP -t 52963b51-ccd0-45b5-814d-043b0fb1a5fb -u nacos -w nacos


-52963b51-ccd0-45b5-814d-043b0fb1a5fb  这个值和registry.conf里namespace一致，对应nacos命名空间的ID，如图
![img.png](img.png)

nacos {
    application = "seata-server"
    serverAddr = "127.0.0.1:8848"
    group = "SEATA_GROUP"
    namespace = "52963b51-ccd0-45b5-814d-043b0fb1a5fb"
    cluster = "default"
    username = "nacos"
    password = "nacos"
}